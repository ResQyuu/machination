﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Machination.Round
{
    public class RoundOrder
    {
        public static RoundOrder GetEmptyRound()
        {
            return new RoundOrder();
        }

        private int CurrentTurnIndex;

        private List<Turn> Turns;

        private RoundOrder()
        {
            Turns = new List<Turn>();
            CurrentTurnIndex = -1;
        }

        public void AddTurn(Turn turn)
        {
            Turns.Add(turn);
            if (Turns.Count == 1)
            {
                CurrentTurnIndex = 0;
                Turns[CurrentTurnIndex].StartTurn();
            }
        }

        public void AdvanceTurn()
        {
            if (Turns.Count > 0)
            {
                GetCurrentTurn().EndTurn();
                AdvanceTurnCountIndex();
                GetCurrentTurn().StartTurn();
            }
            else
            {
                throw new EmptyRoundCycleException();
            }
        }

        public Turn GetCurrentTurn()
        {
            if (Turns.Count > 0)
            {
                return Turns[CurrentTurnIndex];
            }
            else
            {
                throw new EmptyRoundCycleException();
            }
        }

        public Boolean IsRoundNotEmpty()
        {
            return Turns.Count > 0;
        }

        public void AdvanceTurnCountIndex()
        {
            CurrentTurnIndex = (CurrentTurnIndex + 1) % Turns.Count;
        }

        public class EmptyRoundCycleException : Exception { }
    }
}