﻿using Machination.Round.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machination.Round
{
    public class Turn : IComparable<Turn>
    {
        private int Priority;
        private int Modifier;
        private TurnState State;

        public Turn()
        {
            Priority = 0;
            Modifier = 0;
        }

        public Turn(int priority)
        {
            Priority = priority;
            Modifier = 0;
        }

        public Turn(int priority, int tieBreaker)
        {
            Priority = priority;
            Modifier = tieBreaker;
        }

        public void StartTurn()
        {
            State = TurnState.InProgress;
        }

        public void EndTurn()
        {
            State = TurnState.Inactive;
        }

        public bool IsState(TurnState otherState)
        {
            return State.Equals(otherState);
        }

        public int CompareTo(Turn other)
        {
            int sum = (this.Priority - other.Priority);
            if (sum == 0)
            {
                if (this.Modifier > other.Modifier)
                {
                    sum++;
                }
                else if (this.Modifier < other.Modifier)
                {
                    sum--;
                }
                else
                {
                    sum += GetTieBreakValue();
                }
            }
            return sum;
        }

        protected virtual int GetTieBreakValue()
        {
            int randomModifier = new Random().Next(2);
            return new int[] { -1, 1 }[randomModifier];
        }

        public override bool Equals(object obj)
        {
            return obj is Turn turn &&
                   Priority == turn.Priority &&
                   Modifier == turn.Modifier &&
                   State == turn.State;
        }

        public override int GetHashCode()
        {
            var hashCode = -1332251969;
            hashCode = hashCode * -1521134295 + Priority.GetHashCode();
            hashCode = hashCode * -1521134295 + Modifier.GetHashCode();
            hashCode = hashCode * -1521134295 + State.GetHashCode();
            return hashCode;
        }
    }
}