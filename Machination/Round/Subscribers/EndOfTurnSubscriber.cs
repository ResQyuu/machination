﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machination.Round.Subscribers
{
    interface EndOfTurnSubscriber
    {
        void ReceiveEndOfTurn();
    }
}
