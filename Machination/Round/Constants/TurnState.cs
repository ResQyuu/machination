﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machination.Round.Constants
{
    public enum TurnState : int
    {
        Inactive = 0,
        Start = 1,
        InProgress = 2,
        End = 3
    }
}
