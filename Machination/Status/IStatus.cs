﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Machination.Entities;

namespace Machination.Status
{

    interface IStatus
    {

        void Apply(Player source);

        void UpdateDuration();
    }
}
