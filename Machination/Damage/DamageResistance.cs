﻿using Machination.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machination.Damage
{
    public class DamageResistance
    {
        private int ResistanceValue { get; }
        private DamageType ResistanceType { get; }

        public DamageResistance(int damageValue, DamageType damageType)
        {
            this.ResistanceValue = damageValue;
            this.ResistanceType = damageType;
        }
    }
}
