﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Machination.Constants
{
    
    public enum DamageType
    {
       Acid,
       Cold,
       Fire,
       Force,
       Lightning,
       Necrotic,
       Poison,
       Psychic,
       Radiant,
       Thunder,
       Untyped,
       Pure,
       Chaos
    }
}
