﻿using Machination.Round;
using Machination.Round.Constants;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace MachinationTests.Round
{
    [TestClass]
    public class RoundOrderTests
    {
        [TestMethod]
        public void StartsAtFirstTurn()
        {
            Turn FirstTurn = new Turn();
            RoundOrder TestOrder = RoundOrder.GetEmptyRound();
            TestOrder.AddTurn(FirstTurn);
            TestOrder.AddTurn(new Turn());
            Assert.IsTrue(TestOrder.GetCurrentTurn().Equals(FirstTurn));
            Assert.IsTrue(FirstTurn.IsState(TurnState.InProgress));
        }

        [TestMethod]
        public void AdvancesToNextTurn()
        {
            Turn FirstTurn = new Turn();
            Turn SecondTurn = new Turn();
            RoundOrder TestOrder = RoundOrder.GetEmptyRound();
            TestOrder.AddTurn(FirstTurn);
            TestOrder.AddTurn(SecondTurn);
            TestOrder.AdvanceTurn();
            Assert.IsTrue(FirstTurn.IsState(TurnState.Inactive));
            Assert.IsTrue(SecondTurn.IsState(TurnState.InProgress));
        }

        [TestMethod]
        public void RoundResetsToTurnOne()
        {
            Turn FirstTurn = new Turn();
            Turn SecondTurn = new Turn();
            RoundOrder TestOrder = RoundOrder.GetEmptyRound();
            TestOrder.AddTurn(FirstTurn);
            TestOrder.AddTurn(SecondTurn);
            TestOrder.AdvanceTurn();
            TestOrder.AdvanceTurn();
            Assert.IsTrue(FirstTurn.IsState(TurnState.InProgress));
            Assert.IsTrue(SecondTurn.IsState(TurnState.Inactive));
        }

        [TestMethod]
        [ExpectedException(typeof(RoundOrder.EmptyRoundCycleException), "Turn cannot advance in an empty round...")]
        public void TurnAdvanceFailsWithEmptyRound()
        {
            RoundOrder TestOrder = RoundOrder.GetEmptyRound();
            TestOrder.AdvanceTurn();
        }
    }
}