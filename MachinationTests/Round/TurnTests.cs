﻿using Machination.Round;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachinationTests
{
    [TestClass]
    public class TurnTests
    {
        [TestMethod]
        public void TurnComparatorTest()
        {
            Turn TestTurnOne = new Turn(30);
            Turn TestTurnTwo = new Turn(25);
            Assert.AreEqual(5, TestTurnOne.CompareTo(TestTurnTwo));
            Assert.AreEqual(-5, TestTurnTwo.CompareTo(TestTurnOne));
        }

        [TestMethod]
        public void TestTieBreak()
        {
            Turn TestTurnOne = new Turn();
            Turn TestTurnTwo = new Turn();
            Assert.AreNotEqual(0, TestTurnOne.CompareTo(TestTurnTwo));
        }
    }
}